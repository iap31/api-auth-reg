'use strict'

//formato del hash:
//      $2b$10$LfvcrCPsqahXFhgG7d7A2.sBsUV1N87b7xIlYs7C07jB0.QN0cHhq
//      ****-- *****************
const { hash } = require('bcrypt');
const bcrypt = require('bcrypt');

//datos para simulacion...
const miPass = 'miContraseña';
const badPass = 'miOtraContraseña';

//salt = bcrypt.salt(10);
//hash = bcrypt.hash(miPass, salt);
//db.users.update(id,hash, salt);
//db.account.hash.update(id, salt);


//Creamos el salt
bcrypt.genSalt(15, (err, salt) => {
    console.log(`Salt 1: ${salt}`);

    //utilizamos el salt para generar un hash
    bcrypt.hash(miPass, salt, (err, hash) => {
        if(err) console.log(err);
        else console.log(`Hash 1: ${hash}`);
    });
});

//Creamos el hash directamente
bcrypt.hash(miPass, 10, (err, hash) => {
    if(err) console.log(err);
    else {
        console.log(`hash 2: ${hash}`);

        //Comprobamos utilizando la contraseña correcta
        bcrypt.compare(miPass, hash, (err, result)=>{
            console.log(`Result 2.1: ${result}`);
        });

        //Comprobamos utilizando la contraseña incorrecta
        bcrypt.compare(miPass, badPass, (err, result)=>{
            console.log(`Result 2.2: ${result}`);
        });
    };
});
