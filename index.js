'use strict'

const port = process.env.PORT || 3000;

const https = require('https');
const fs = require('fs');

const OPTIONS_https = {
    key: fs.readFileSync('./cert/key.pem'),
    cert: fs.readFileSync('./cert/cert.pem')
};

//Token
const tokenService = require('./services/token.service');
const PassService = require('./services/pass.services');
const res = require('./node_modules/express/lib/response');

const moment = require('moment');
const logger = require("morgan");
const express = require('express');
const mongojs = require('mongojs');
const cors = require('cors');

var db = mongojs("SD");
var id = mongojs.ObjectId;

var allowCrossTokenHeader = (req, res, next) =>{
    res.header("Access-Control-Allow-Headers", "*");
    return next();
};
var allowCrossTokenOrigin = (req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    return next();
};
var allowMethods = (req, res, next) => {
    res.header("Access-Control-Allow-Methods", "GET, POS, PUT, DELETE, OPTIONS");
    return next();
};
function auth (req, res, next) {
    const jwt = req.headers.authorization.split(" ")[1];
    console.log(jwt);
    tokenService.decodificaToken(jwt).then(tokendd => {
        console.log(tokendd);
        if(tokendd) {
            req.user = {
                id: tokendd
            };
            return next();
        }else {
            res.status(400).json({
                error: 'Bad data',
                description: 'token no valido'
            });
        }
    }).catch(err => {
        res.status(400).json(err);
    })
        
};

//var db = mongojs('username:password@example.com/SD');

const app = express();
//Declarar midleware
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended:false}));

app.use(cors());
app.use(allowCrossTokenHeader);
app.use(allowCrossTokenOrigin);
app.use(allowMethods);




// rutas

app.get('/api', (req, res, next) => {
    console.log('GET /api');
    console.log(req.params);
    console.log(req.collection);

    db.getCollectionNames((err, usuarios) => {
        if(err) return next(err);
        res.json(usuarios);
    });
});

app.get('/api/user', (req, res, next) => {
    db.user.find((err, user) => {
        if(err) return next(err);
        res.json(user);
    })
});

app.get('/api/user/:id', (req, res, next) => {
    db.user.findOne({_id: id(req.params.id)}, (err, elemento) => {
        if(err) return next(err);
        res.json(elemento);
    });
});

app.post('/api/user',auth, (req,res,next) => {
    const elemento = req.body;

    if(!elemento.nombre) {
        res.status(400).json ({
            error: 'Bad data',
            description: 'Se precisa al menos un campo <nombre>'
        });
    } else {
        db.user.save(elemento, (err, coleccionGuardada) => {
            if(err) return next(err);
            res.json(coleccionGuardada);
        });
    }
});

app.put('/api/user/:id', auth, (req,res, next) => {
    let elementoId = req.params.id;
    let elementoNuevo = req.body;
    db.user.update({_id: id(elementoId)},
        {$set: elementoNuevo}, {safe: true, multi:false}, (err, elementoModif)=> {
            if(err) return next(err);
            res.json(elementoModif);
    });
});

app.delete('/api/user/:id',auth, (req,res, next) => {
    let elementoId = req.params.id;

    db.user.remove({_id: id(elementoId)}, (err, resultado) => {
        if(err) return next(err);
        res.json(resultado);
    });
});

app.get('/api/auth', (req,res, next) => {
    db.user.find({}, {name: 1, email: 1}, (err, user) => {
        if(err) return next(err);
        res.json(user);
    });
});

app.get('/api/auth/me', auth, (req, res, next) => {
    db.user.findOne({ _id: id(req.user.id)}, (err, elem) => {
        if (err) return next(err);
        res.json(elem);
    });
});

app.post('/api/auth', (req, res, next) => {
    const elemento = req.body;
    if(!elemento.email || !elemento.pass) {
        res.status(400).json({
            error: 'Bad data',
            description: 'No tenemos ese usuario'
        });
    }else {
        db.user.findOne({ email: elemento.email}, (err, usuEn) => {
            if(err) return next(err);
            if(!usuEn){
                res.status.json({
                    error: 'Bad data',
                    description: 'El email no existe'
                });
            }else {
                PassService.comparaPassword(elemento.pass, usuEn.pass).then(passBien => {
                    if(passBien){
                        PassService.encriptaPassword(usuEn.pass).then(hash => {
                            usuEn.pass = hash;
                            const token = tokenService.creaToken(usuEn);
                            const u = {
                                name: usuEn.name,
                                email:usuEn.email,
                                pass: usuEn.pass,
                                signUpDate: moment().unix(),
                                lastLogin: moment().unix(),
                                _id: id(req.params.id)
                            }
                            if(err) return next(err);
                            res.json({
                                result: "ok",
                                token: token,
                                usuario: u
                            });
                        })
                    }else {
                        res.status(400).json({
                            error: 'Bad data',
                            description: 'Contraseña incorrecta'
                        });
                    }
                });
            }
        });
    }
});

app.post('/api/reg', (req, res, next) => {
    const elemento = req.body;

    if(!elemento.name || !elemento.email || !elemento.pass){
        res.status(400).json({
            error: 'Bad data',
            description: 'Eñ email ya no existe'
        });
    }else{
        db.user.findOne({email: elemento.email}, (err, email) => {
            if(err) return next(err);
            if(email){
                res.status(400).json({
                    error: 'Bad data',
                    description: 'El email ya no existe'
                });
            }else {
                PassService.encriptaPassword(elemento.pass).then(hash => {
                    elemento.pass = hash;

                    const u = {
                        name: elemento.name,
                        email: elemento.email,
                        pass: elemento.pass,
                        signUpDate: moment().unix(),
                        lastLogin: moment().unix(),
                        _id: id(req.params.id)
                    }
                    db.user.save(elemento, (err, userGuardada)=> {
                        const token = tokenService.creaToken(elemento);
                        if(err) return next(err);
                        res.json({
                            result: "Ok",
                            token: token,
                            usuario: u
                        });
                    });
                });
            }
        });
    }
});

https.createServer(OPTIONS_https, app).listen(port, () => {
    console.log(`SEC WS APE REST ejecutándose en https://localhost:${port}/api/:coleccion/:id`);
});


//Lanzamos el servicio
/*app.listen(port, () => {
    console.log(`API REST ejecutándose en http://localhost:${port}/api/:coleccion/:id`);
});*/
